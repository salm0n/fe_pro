let numbers = document.querySelectorAll('.number');
let action = document.querySelectorAll('.pink');
let display = document.querySelector('.display>input');

//
let m = document.createElement('span');
m.style.position = 'absolute';
m.style.top = '2px';
m.style.left = '6px';
m.style.zIndex = '1';
document.querySelector('.display').prepend(m);
m.classList.add('memory');
m.style.display = 'none';
//

let a = '';
let b = '';
let sign = '';
let memory = '';
let clearMemory = false;
let equal = false;

for (let i = 0; i < numbers.length; i++){
    numbers[i].onclick = () => {
        if (b === '' && sign === '') {
            a = display.value += numbers[i].value;
        }else if (a === !'' && b === !'' && equal){
            b = display.value = numbers[i].value;
            equal = false;

            display.value = a;
        }else{
            if (display.value === '+' || display.value === '-' || display.value === '/' || display.value === '*'){
                display.value = '';
            }
            b = display.value += numbers[i].value;
        }
    };
}

const reset = () => {
    a = '';
    b = '';
    sign = '';
    display.value = '';
}

document.querySelector('input[value = "C"]').addEventListener('click', reset);

for (let i = 0; i < action.length; i++){
    action[i].onclick = () => {
        sign = display.value = action[i].value;
    }
}

document.querySelector('.orange').onclick = () => {

    switch (sign){
        case '+':
            a = (+a) + (+b);
            break;
        case '-':
            a = a - b;
            break;
        case '*':
            a = a * b;
            break;
        case '/':
            a = b === '0' ? 'помилка' : a / b;
            break;
    }

    display.value = a;

    equal = true;
}

document.querySelector('input[value = "m+"]').addEventListener('click',() => {

    memory = memory ===''? memory = display.value : memory = Number(memory) + Number(display.value);

    m.style.display = 'block';
    m.innerText = 'm+';
});

document.querySelector('input[value = "m-"]').addEventListener('click',() => {

    memory = memory ===''? memory = '-' + display.value : memory = Number(memory) + Number(display.value);

    m.style.display = 'block';
    m.innerText = 'm-';

});

document.querySelector('input[value = "mrc"]').addEventListener('click', () => {

    if (!clearMemory){
        display.value = memory;
        clearMemory = true;
    }else if(clearMemory === true && display.value === memory){
        reset();
        memory = '';
        clearMemory = false;
        m.style.display = 'none';
    }else{
        display.value = memory;
        clearMemory = false;
    }
});