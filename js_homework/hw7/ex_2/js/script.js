class CreateUser {
    constructor(name, lastName, birthday) {
        this.firstName = name;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    getLogin () {
        return (this.firstName.substr(0, 1).toLowerCase() + this.lastName.toLowerCase());
    }

    getAge () {
        let yearNow = new Date();
        let year = yearNow.getFullYear();
        return (year - this.birthday.substr(6, 9));
    }

    getPassword () {
        return (this.firstName.substr(0, 1).toUpperCase()) + this.lastName.toLowerCase() + this.birthday.substr(6,9);
    }
}

const user1 = new CreateUser(prompt('Введите имя'), prompt('Введите фамилию'), prompt('дата рождения', 'dd.mm.yyyy'));

console.log(user1.getLogin());
console.log(user1.getAge());
console.log(user1.getPassword());