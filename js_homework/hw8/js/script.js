document.querySelector('.button').style = 'display: block; margin: 0 auto';

document.querySelector('.button').onclick = function (){
    document.write(`<input id="write_diameter" placeholder = "диаметр круга (px)">
    <input class="circle_draw" type="button" value="нарисовать">`);

    let input = document.querySelector('#write_diameter');
    document.querySelector('.circle_draw').style = 'display: block; margin: 20px auto';
    input.style = 'display: block; margin: 0 auto';

    document.querySelector('.circle_draw').onclick = function () {
        for (let i = 0; i < 100; i++) {
            let circle = document.createElement('div');

            document.body.append(circle);

            circle.style.display = 'inline-block'
            circle.style.borderRadius = '50%';
            circle.style.margin = '20px';
            circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;

            let diameter = input.value;

            circle.style.width = diameter;
            circle.style.height = diameter;

            circle.onclick= function (){
                circle.remove();
            }

        }
    }
}

