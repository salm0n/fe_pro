
let styles = ["jazz", "blues",];

styles.push("rock and roll");   // додаємо в кінець новий елемент

styles[Math.floor(styles.length/2)] = "classic"; //  змінюємо значення в середині

document.write(styles + "</br>");

let first = styles.shift();                                                                      //  видаляємо перший елемент

document.write("перший видалений елемент: " + first + "</br>");                                           //  виводимо його на екран

styles.unshift("rap", "reggae");                                   // додаємо два елемента на початок

document.write(styles);
