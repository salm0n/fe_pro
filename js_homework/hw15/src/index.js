import React from 'react';
import ReactDOM from 'react-dom';


const Week = () => {
    return (
        <div>
            <h2>days of the week</h2>
            <ul>
                <li>Monday</li>
                <li>Tuesday</li>
                <li>Wednesday</li>
                <li>Thursday</li>
                <li>Friday</li>
                <li>Saturday</li>
                <li>Sunday</li>
            </ul>
        </div>
    )
}

const Zodiac = () => {
    return (
        <div>
            <h2>Zodiac</h2>
            <ul>
                <li>Aries</li>
                <li>Taurus</li>
                <li>Gemini</li>
                <li>Cancer</li>
                <li>Leo</li>
                <li>Virgo</li>
                <li>Libra</li>
                <li>Scorpius</li>
                <li>Sagittarius</li>
                <li>Capricorn</li>
                <li>Aquarius</li>
                <li>Pisces</li>
            </ul>
        </div>
    )
}

const Months = () => {
    return (
        <div>
            <h2>Months</h2>
            <ul>
                <li>January</li>
                <li>February</li>
                <li>March</li>
                <li>April</li>
                <li>May</li>
                <li>June</li>
                <li>July</li>
                <li>August</li>
                <li>September</li>
                <li>October</li>
                <li>November</li>
                <li>December</li>
            </ul>
        </div>
    )
}

const App = function (){
    return (
        <div>
            <Months></Months>
            <Week></Week>
            <Zodiac></Zodiac>
        </div>
    )
}

ReactDOM.render(<App></App>, document.querySelector('#root'));