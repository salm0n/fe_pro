const get = attr => document.querySelector(attr);


let pizza = get('.table');
let pizzaPrice = 0;
let arrProd = [];
let prodSum = 0;
let prodPrics = {
    'sauceClassic': 17,
    'sauceBBQ': 17,
    'sauceRikotta': 17,
    'moc1': 25,
    'moc2': 28,
    'moc3': 27,
    'telya': 32,
    'vetch1': 22,
    'vetch2': 24,
};


class User{
    constructor(name, phone, mail) {
        this.name = name;
        this.phone = phone;
        this.mail = mail;
    }
}


//============================================== check pizza size====================================================
let radioBtns = get('#pizza');


radioBtns.onchange = () => {
    if (get('#small').checked){
        pizzaPrice = 80;

    }else if (get('#mid').checked){
        pizzaPrice = 120;

    }else if (get('#big').checked){
        pizzaPrice = 160;

    }

    get('.price>p').innerHTML = `ЦIНА: ${pizzaPrice + prodSum} грн`;
}


//================================================drag and drop===================================================

let [...products] = document.querySelectorAll('.draggable');


products.forEach(el => {
    el.ondragstart = e => {
        e.dataTransfer.setData('id', e.target.id);
        e.dataTransfer.setData('info', el.nextElementSibling.innerHTML);
    }
});


pizza.ondragover = e => {
    e.preventDefault();
};


pizza.ondrop = e => {
    let id = e.dataTransfer.getData('id');
    let info = e.dataTransfer.getData('info');
    let prod = document.getElementById(id);
    let copy = prod.cloneNode();


    if(pizza.appendChild(copy)){
        prod.draggable = false;
        prod.style.opacity = '.3';
    }


    if (/^sauce/.test(copy.id)){
        get('.sauces>p').innerHTML += `<div class=${copy.id}>${info}</div>`;
    }else{
        get('.topings>p').innerHTML += `<div class=${copy.id}>${info}</div>`;
    }


    arrProd.push(copy);



    for (key in prodPrics){

        if (copy.id === key){

            prodSum += prodPrics[key];

            get('.price>p').innerHTML = `ЦIНА: ${pizzaPrice + prodSum} грн`;

            //   Видалення топінгів і соусів

            let [...prodDel] = document.querySelectorAll('.result>div div');

            prodDel.forEach(el => {
                el.onclick = () => {

                    for (let i = 0; i < arrProd.length; i++){

                        if (el.classList.value === arrProd[i].id) {

                            document.querySelectorAll('.ingridients img[draggable="false"]').forEach(el => {
                                if (arrProd[i].id === el.id){
                                    el.setAttribute('draggable','true');
                                    el.style.opacity = '1';
                                }
                            })

                            get(`.table>img[id=${el.classList.value}]`).remove()

                            console.log(arrProd.splice(i, 1));

                            el.remove();

                            for (key in prodPrics){
                                if (key === el.classList.value){
                                    prodSum -= prodPrics[key];
                                    get('.price>p').innerHTML = `ЦIНА: ${pizzaPrice + prodSum} грн`;
                                }
                            }
                        }
                    }
                }
            })
        }
    }
}

//                        Валідація

localStorage.user = JSON.stringify([]);
let [...formInp] = document.querySelectorAll('.grid>input:not([class="button"])');


const validate = e => {
    switch (e.name){
        case 'name': return /^[A-zА-я ]{2,}$/i.test(e.value);
        case 'email': return /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i.test(e.value);
        case 'phone': return /^\+380\d{9}$/.test(e.value);
        default: throw new Error('Невірний виклик регулярного виразу');
    }
}


formInp.forEach(el => {

    el.onchange = e => {
        if (!validate(e.target)){
            e.target.classList.add('error');
            e.target.classList.remove('success');

        }else{
            e.target.classList.add('success');
            e.target.classList.remove('error');
        }
    };
})


get('input[name="btnSubmit"]').onclick = () => {
    let validateRez = formInp.map( el => {
        return validate(el);
    });


    if (!validateRez.includes(false)){
        document.location = './thank-you.html';
        let a = JSON.parse(localStorage.user);
        a.push(new User(...formInp.map(element => {
            return element.value;
        })), `price: ${pizzaPrice + prodSum} UAH`);
        localStorage.user = JSON.stringify(a);
    }else{
        alert('Помилка при введені даних!');
    }

}

get('input[name="cancel"]').onclick = () => {
    formInp.forEach(el => {
        el.classList.remove('error');
        el.classList.remove('success');
    });
};

//     Тікаюча знижка

let banner = document.querySelector('#banner');

banner.addEventListener('mouseover', e => {
    banner.style.transition = '.3s';
    banner.style.bottom = `${Math.floor(Math.random() * e.clientY)}px`;
    banner.style.right = `${Math.floor(Math.random() * e.clientX)}px`;
});