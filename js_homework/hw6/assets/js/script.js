function Human (name, age){
    this.name = name;
    this.age = age;

    this.sort = function (arr){
        arr.sort();
    }
}

let arr = [];

arr.push(new Human('John', 23));
arr.push(new Human('Mike', 54));
arr.push(new Human('Sara', 31));
arr.push(new Human('Leo', 15));
arr.push(new Human('Mira', 87));


function inc (a, b){
    return a.age > b.age ? 1 : a.age < b.age ? -1 : 0;

}

arr.sort(inc);

arr.forEach(element => console.log(element));
