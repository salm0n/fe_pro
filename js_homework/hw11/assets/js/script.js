let task = document.querySelectorAll('.task');

// Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища
class CreateUser {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

    }
    show() {
        document.getElementById('showName').innerText = `${this.firstName} ${this.lastName}`;
    }

}

let user1 = new CreateUser('Mark', 'Zuckerberg');

user1.show();


//Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний

let li2 = document.querySelector('ul>li:nth-child(2)');

li2.previousElementSibling.style = 'background-color: blue';

li2.nextElementSibling.style = 'background-color: red';


//Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

let display = document.createElement('div');
document.querySelector('#block').append(display);

const info = e => display.innerText = `Coordinates: X: ${e.clientX} Y: ${e.clientY}`;

document.querySelector('#block').addEventListener('mousemove', info);


//Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута
let infNum = document.createElement('div');
task[3].append(infNum);

for (let i = 0; i < 4; i++) {
    task[3].append(document.createElement('input'));
    let inp = document.querySelectorAll('input');
    inp[i].value = `${i + 1}`;
    inp[i].type = 'button';
    inp[i].style.margin = '30px';
    inp[i].style.padding = '0 20px';
    inp[i].onclick = e => infNum.innerText = `Була натиснута кнопка під номером ${e.target.value}`;
}


// Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці
const div = document.createElement('div');
document.querySelector('.space').append(div);

let divMove = document.querySelector('.space>div');

divMove.addEventListener("mouseover", () => {
    divMove.style.left = `${Math.floor(Math.random() * 400)}px`;
    divMove.style.top = `${Math.floor(Math.random() * 400)}px`;
})


//Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body
let inp = document.createElement('input');
let btn = document.createElement('input');
btn.type = 'button';

task[5].append(inp);
task[5].append(btn);
btn.value = 'Підтвердити';

btn.onclick = () => document.body.style.backgroundColor = `${inp.value}`;


//Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль

let inp2 = document.createElement('input');

task[6].append(inp2);

inp2.oninput = () => console.log(inp2.value);


//Створіть поле для введення даних у полі введення даних виведіть текст під полем

let inp3 = document.createElement('input');
let out = document.createElement('div');

task[7].append(inp3);
task[7].append(out);

inp3.oninput = () => out.innerText = `${inp3.value}`;
