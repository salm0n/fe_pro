let offset = 0;
const sliderLine = document.querySelector('.slider>div:first-child');

const nextSlide = () => {
    offset = offset + 700;
    if (offset > 2800){
        offset = 0;
    }
    sliderLine.style.left = -offset + 'px';
}

setInterval(nextSlide, 3000);