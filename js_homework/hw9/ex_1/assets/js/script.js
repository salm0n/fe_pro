let sec = 0, intervalHandler;
let min = 0;
let hrs = 0;

let display = document.querySelector('#display');

const get = id => document.getElementById(id);

const zeroReg = x => x < 10 ? '0' + x : x;

const count = () => {
    get("display").innerText = `${zeroReg(hrs)}:${zeroReg(min)}:${zeroReg(sec)}`;

    sec++;

    if(sec > 59){
        sec = 0;
        min++;

    }
    if(min > 59){
        min = 0;
        hrs++;
    }
}

get('start__btn').onclick = (e) => {
    intervalHandler = setInterval(count, 1000);
    display.classList.add('green');
}

get('stop__btn').onclick = () => {
    clearInterval(intervalHandler);
    display.classList.add('red');
    display.classList.remove('green');
}

get('reset__btn').onclick = () => {
    sec = 0;
    min = 0;
    hrs = 0;
    get("display").innerText = `${zeroReg(hrs)}:${zeroReg(min)}:${zeroReg(sec)}`;

    clearInterval(intervalHandler);
    display.classList.add('silver');
    display.classList.remove('green', 'red');
}


