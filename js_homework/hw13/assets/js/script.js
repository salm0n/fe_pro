let arrPeople = [];
let save = [];


class Character {
    constructor(name, gender, height, homeWorld, skinColor, birthYear) {
        this.name = name;
        this.gender = gender;
        this.height = height;
        this.homeWorld = homeWorld;
        this.skinColor = skinColor;
        this.birthYear = birthYear;
    }

    createCard(){
        const card = document.createElement('div');
        const cardName = document.createElement('div');
        const cardGender = document.createElement('div');
        const cardHeight = document.createElement('div');
        const cardHomeWorld = document.createElement('div');
        const cardSkinColor = document.createElement('div');
        const cardYear = document.createElement('div');
        const btn = document.createElement('button');
        card.setAttribute('class','card');

        cardName.innerText = `Name: ${this.name}`;
        cardGender.innerText = `Gender: ${this.gender}`;
        cardHeight.innerText = `Height: ${this.height}sm`;
        cardHomeWorld.innerText = `Homeworld: ${this.homeWorld}`;
        cardSkinColor.innerText = `skin color: ${this.skinColor}`;
        cardYear.innerText = `Birth year: ${this.birthYear}`;
        btn.innerText = 'save';
        btn.setAttribute('id', this.name);

        document.querySelector('.wrapper').append(card);

        card.append(btn, cardName, cardGender, cardHeight, cardHomeWorld, cardSkinColor, cardYear);

        const div = document.createElement('div');
        div.setAttribute('class', 'card');

        card.onclick = e => {
            if (e.target === btn){
                arrPeople.forEach(el => {
                    if(el.name === e.target.id){
                        save.push(el);
                        localStorage.setItem('saved', JSON.stringify(save));
                        card.style.backgroundColor = '#fff';
                    }
                })
            }
        }
    }
}

const url = 'https://swapi.dev/api/people';

fetch(url)
    .then((rez) => rez.json())
    .then((rez) => {
        rez.results.forEach(element => {

            let user = new Character(element.name, element.gender, element.height, element.homeworld, element.skin_color, element.birth_year);
            user.createCard();
            arrPeople.push(user);
        });
    });
