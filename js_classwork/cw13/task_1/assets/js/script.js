const url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';
let req = new XMLHttpRequest();


req.open('GET', url);
req.addEventListener('readystatechange', () => {

    if (req.readyState === 4 && req.status >= 200 && req.status < 300){
        showCurrency(JSON.parse(req.responseText));
    }
});


req.send()


function showCurrency(arr) {

    arr.forEach(element => {
        const {txt, rate} = element;

        console.log(txt);

        const tr = document.createElement('tr');
        const txtTd = document.createElement('td');
        const rateTd = document.createElement('td');

        txtTd.innerText = txt;
        rateTd.innerText = rate;

        tr.append(txtTd, rateTd);
        document.querySelector('tbody').append(tr);

    });
}
