import React from "react";
import './table.css'


function Table(props) {
  return (
    <table>
      <tbody>
        {props.data.map((element) => {
          return <tr>
            <td>{element.r030}</td>
            <td>{element.cc}</td>
            <td>{element.txt}</td>
            <td>{element.rate}</td>
          </tr>
        })}
      </tbody>
    </table>
  );
}



export default Table